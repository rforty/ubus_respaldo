package facci.ronny_forty.ubus.Vista.LoginView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.ronny_forty.ubus.Vista.AdminView.Administrador;
import facci.ronny_forty.ubus.Presentador.LoginPresenter.PresentadorLogin;
import facci.ronny_forty.ubus.R;
import facci.ronny_forty.ubus.Vista.RegisterView.Register;


public class Login extends AppCompatActivity implements View.OnClickListener {
    private EditText mEtxtEmail, mEtxtPassword;
    private PresentadorLogin presentadorLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        presentadorLogin = new PresentadorLogin(this, mAuth, mDatabase);
        mEtxtEmail = findViewById(R.id.txt_email);
        mEtxtPassword = findViewById(R.id.txt_psswd);

        SharedPreferences preferences = getSharedPreferences("datos", Context.MODE_PRIVATE);
        mEtxtEmail.setText(preferences.getString("mail",""));



        Button mBtnLogin = findViewById(R.id.btn_login);
        mBtnLogin.setOnClickListener(this);

        Button mBtnRegistrar =findViewById(R.id.button_register);
        mBtnRegistrar.setOnClickListener(this);

        ImageButton mBtnAdmin = findViewById(R.id.button_Admi);
        mBtnAdmin.setOnClickListener(this);





    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_login:
                String email = mEtxtEmail.getText().toString().trim();
                String pass = mEtxtPassword.getText().toString().trim();

                SharedPreferences preferencias = getSharedPreferences("datos", Context.MODE_PRIVATE);
                SharedPreferences.Editor Obj_editor = preferencias.edit();
                Obj_editor.putString("mail",mEtxtEmail.getText().toString());
                Obj_editor.commit();

                presentadorLogin.ComprobarCamposllenos(email, pass);
                break;


            case R.id.button_register: Intent intent = new Intent (Login.this, Register.class);
            startActivity(intent);
            break;


            case R.id.button_Admi: Intent intent2 = new Intent( Login.this, Administrador.class);
            startActivity(intent2);



        }


    }


}

