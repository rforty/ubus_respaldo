

package facci.ronny_forty.ubus.Vista.MenuView;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import facci.ronny_forty.ubus.Presentador.MainPresenter.PresentadorMain;
import facci.ronny_forty.ubus.R;
import facci.ronny_forty.ubus.destino_casa;
import facci.ronny_forty.ubus.destino_universidad;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button button_home;
    Button button_university;
    TextView tv_user1;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    PresentadorMain presentadorMain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_user1 =findViewById(R.id.tv_user);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        button_home = findViewById(R.id.button_home);
        button_home.setOnClickListener(this);

        button_university = findViewById(R.id.button_university);
        button_university.setOnClickListener(this);


        presentadorMain = new PresentadorMain();
        presentadorMain.SaludarUsuario(mDatabase,mAuth,MainActivity.this, tv_user1);



    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){

            case R.id.button_home : Intent intent = new Intent(MainActivity.this,destino_casa.class);
            startActivity(intent);
            break;

            case R.id.button_university : Intent intent2 = new Intent(MainActivity.this, destino_universidad.class);
            startActivity(intent2);
            break;

        }

    }
}
