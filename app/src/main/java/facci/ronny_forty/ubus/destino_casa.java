package facci.ronny_forty.ubus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import facci.ronny_forty.ubus.R;

public class destino_casa extends AppCompatActivity {

    private TextView asientos_dis;
    private Button btn_reservar;
    private int num_asientos=14;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destino_casa);

        asientos_dis = (TextView)findViewById(R.id.txt_disponibles);

    }
    public void reservar (View view){
        num_asientos=num_asientos-1;
        String num_conver = String.valueOf(num_asientos);
        asientos_dis.setText(num_conver);

        Toast.makeText(this,"Asiento Reservado",Toast.LENGTH_SHORT).show();


    }
}
